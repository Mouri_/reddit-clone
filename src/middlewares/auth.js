const httpStatus = require("http-status");

module.exports = (req, res, next) => {
  if (!req.headers.hasOwnProperty("authorization")) {
    res.status(httpStatus.UNAUTHORIZED).json({ error: "Token required" });
  } else if (req.body.loggedin) {
    next();
  } else {
    res.status(httpStatus.UNAUTHORIZED).json({ error: "Invalid token" });
  }
};
