const jwt = require("jsonwebtoken");
const config = require("../config/config");

module.exports = (req, res, next) => {
  if (req.headers.hasOwnProperty("authorization")) {
    const token = req.headers.authorization.replace("Bearer ", "");
    jwt.verify(token, config.jwt.secret, (err, result) => {
      if (!err) {
        req.body.user_name = result;
        req.body.loggedin = true;
      } else {
        req.body.loggedin = false;
      }
    });
  } else {
    req.body.loggedin = false;
  }
  next();
};
