module.exports = `<div style="font-family: Helvetica,Arial,sans-serif;max-width:1000px;overflow:auto;line-height:2">
<div style="margin:50px auto;width:70%;padding:20px 0">
  <div style="border-bottom:1px solid #eee">
    <a href="" style="font-size:1.4em;color: red;text-decoration:none;font-weight:600">Reddit-clone</a>
  </div>
  <p style="font-size:1.1em">Hi there,</p>
  <p>Your email address {email} has been added to your {name} Reddit-clone account. But wait, we’re not done yet...<br />

  To finish verifying your email address and securing your account, click the button below.</p>
  <a href="{link}" style="text-decoration: none;"><h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 5px 20px;color: #fff;border-radius: 4px;">Verify</h2></a>
</div>
</div>`;
