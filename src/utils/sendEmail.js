const nodemailer = require("nodemailer");
const emailCred = require("../config/config").email;

async function sendEmail(receiver, subject, message) {
  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport(emailCred.smtp);

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: `"" <${emailCred.from}>`,
    to: `${receiver}`,
    subject: `${subject}`,
    html: `${message}`,
  });
}

module.exports = sendEmail;
