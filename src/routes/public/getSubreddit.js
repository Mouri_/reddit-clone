const subredditModel = require("../../models").subReddit;
const router = require("express").Router();
const httpStatus = require("http-status");

router.get("/", (req, res) => {
  subredditModel
    .findAll({
      raw: true,
    })
    .then(result => res.status(httpStatus.OK).json(result || []))
    .catch(err => res.status(httpStatus.BAD_REQUEST).json(err));
});

router.get("/:url", (req, res) => {
  subredditModel
    .findByPk(req.params.url)
    .then(result =>
      result
        ? res.status(httpStatus.OK).json(result)
        : res
            .status(httpStatus.BAD_REQUEST)
            .json({ error: "Subreddit does not exist" })
    )
    .catch(err => res.status(httpStatus.BAD_REQUEST).json(err));
});

module.exports = router;
