const usersModel = require("../../models/users");
const router = require("express").Router();
const jwt = require("jsonwebtoken");
const emailTemplate = require("../../utils/verificationEmailTemplate");
const sendEmail = require("../../utils/sendEmail");
const bcrypt = require("bcrypt");
const httpStatus = require("http-status");
const config = require("../../config/config");
const validate = require("../../validation/index").validateSignup;

router.post("/", async (req, res) => {
  const body = req.body;

  const error = {
    user_name: validate.userName({ username: body.user_name || "" }),
    password: validate.password({ password: body.password || "" }),
    email: validate.email({ email: body.email || "" }),
  };

  await checkInDB("user_name", body.user_name || "", error);
  await checkInDB("email", body.email || "", error);

  if (Object.values(error).some(err => err)) {
    return res.status(httpStatus.BAD_REQUEST).json({ error });
  }

  const salt = bcrypt.genSaltSync();
  body.password = bcrypt.hashSync(body.password, salt);

  usersModel
    .create(body)
    .then(result => res.status(httpStatus.OK).json({ result: "Success" }))
    .then(res => sendVerification(body.email, body.user_name))
    .catch(err =>
      res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({ error: { unknown: "Something went wrong" } })
    );
});

async function checkInDB(prop, value, error) {
  if (error.hasOwnProperty(prop)) {
    await usersModel
      .findAll({
        attributes: ["user_name"],
        where: {
          [prop]: value,
        },
      })
      .then(result => {
        if (result.length !== 0)
          error[prop] = `${prop.replace("_", "")} already exists`;
      });
  }
}

function sendVerification(email, user) {
  const token = jwt.sign(user, config.jwt.secret);
  const link = `${config.api.url}/verification/${token}`;
  const subject = `Welcome to reddit-clone`;
  const message = emailTemplate
    .replace("{email}", `${email}`)
    .replace("{name}", `${user}`)
    .replace("{link}", `${link}`);
  sendEmail(email, subject, message);
}

module.exports = router;
