const usersModel = require("../../models").users;
const router = require("express").Router();
const httpStatus = require("http-status");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const validate = require("../../validation/index").validateLogin;
const config = require("../../config/config");

router.post("/", (req, res) => {
  const body = req.body;
  const user_name = body.user_name;
  const password = body.password;

  const error = {
    user_name: validate.userName({ username: user_name }),
    passowrd: validate.password({ password }),
  };

  if (Object.values(error).some(err => err)) {
    return res.status(httpStatus.BAD_REQUEST).json({ error });
  }

  usersModel
    .findAll({
      attributes: ["password"],
      where: {
        user_name: user_name,
      },
    })
    .then(result => {
      if (result.length <= 0)
        return res
          .status(httpStatus.BAD_REQUEST)
          .json({ error: { user_name: "Wrong user name" } });
      const user = result[0];
      if (bcrypt.compareSync(password, user.password)) {
        const token = jwt.sign(user_name, config.jwt.secret);
        res.status(httpStatus.OK).json({ token: token });
      } else {
        res
          .status(httpStatus.BAD_REQUEST)
          .json({ error: { password: "Wrong password" } });
      }
    })
    .catch(err => {
      res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({ error: { unknown: "Something went wrong" } });
    });
});

module.exports = router;
