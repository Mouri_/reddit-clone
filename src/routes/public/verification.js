const usersModel = require("../../models/users");
const router = require("express").Router();
const httpStatus = require("http-status");
const jwt = require("jsonwebtoken");
const config = require("../../config/config");
const successMessage = require("../../utils/successMessage");

router.get("/:token", (req, res) => {
  jwt.verify(req.params.token, config.jwt.secret, (err, result) => {
    if (err) {
      return res
        .status(httpStatus.BAD_REQUEST)
        .send(successMessage.replace("{status}", "Unsuccessful"));
    }
    usersModel
      .update(
        { verification: true },
        {
          where: {
            user_name: result,
          },
        }
      )
      .then(result =>
        res
          .status(httpStatus.OK)
          .send(successMessage.replace("{status}", "Successful"))
      )
      .catch(err =>
        res
          .status(httpStatus.BAD_REQUEST)
          .send(successMessage.replace("{status}", "Unsuccessful"))
      );
  });
});

module.exports = router;
