const postsModel = require("../../models").posts;
const usersModel = require("../../models").users;
const router = require("express").Router();
const httpStatus = require("http-status");
const Op = require("sequelize").Op;

router.get("/", async (req, res) => {
  const query = req.query;
  const sortBy = query.sort === "new" ? "createdAt" : null || "top"; // top hot new
  const order = "DESC";
  const country = query.country;
  const page = parseInt(query.page) || 0;
  const perPage = parseInt(query.perpage) || 5;
  const onlyJoined = query.onlyjoined ? JSON.parse(query.onlyjoined) : true;
  const loggedin = req.body.loggedin;
  const subReddit = query.subreddit;

  const condition = {};
  if (country) condition.country = country;

  if (onlyJoined && loggedin) {
    condition.sub_reddit = {
      [Op.in]: await usersModel
        .findByPk(req.body.user_name, {
          attributes: ["joined_sub_reddit"],
          raw: true,
        })
        .then(res => JSON.parse(res ? res.joined_sub_reddit : null))
        .catch(err => null),
    };
  }

  if (subReddit) condition.sub_reddit = subReddit;

  postsModel
    .count({
      where: condition,
    })
    .then(count => {
      return {
        no_of_pages: Math.ceil(count / perPage),
        posts_per_page: perPage,
        page: page,
        no_of_posts: count,
      };
    })
    .then(async res => {
      res.posts = await postsModel.findAll({
        where: condition,
        offset: perPage * page,
        limit: perPage,
        order: [[sortBy, order]],
      });
      return res;
    })
    .then(results => {
      res.status(httpStatus.OK).json(results);
    })
    .catch(err => {
      res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({ error: "Something went wrong" });
    });
});

module.exports = router;
