const router = require("express").Router();

router.use("/login", require("./login"));
router.use("/signup", require("./signup"));
router.use("/verification", require("./verification"));
router.use("/getposts", require("./getPosts"));
router.use("/getsubreddits", require("./getSubreddit"));
router.use("/getcomments", require("./getComments"));

module.exports = router;
