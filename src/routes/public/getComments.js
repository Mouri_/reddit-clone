const commentsModel = require("../../models").comments;
const router = require("express").Router();
const httpStatus = require("http-status");

router.get("/:id", (req, res) => {
  commentsModel
    .findAll({
      where: {
        belongs_to: req.params.id,
      },
    })
    .then(result =>
      result
        ? res.status(httpStatus.OK).json(result)
        : res
            .status(httpStatus.BAD_REQUEST)
            .json({ error: "Subreddit does not exist" })
    )
    .catch(err => res.status(httpStatus.BAD_REQUEST).json(err));
});

module.exports = router;
