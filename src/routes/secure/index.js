const router = require("express").Router();

router.use("/createpost", require("./createPost"));
router.use("/createsubreddit", require("./createSubreddit"));
router.use("/createcomment", require("./createComment"));
router.use("/interact", require("./interact"));

module.exports = router;
