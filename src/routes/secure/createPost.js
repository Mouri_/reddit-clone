const postsModel = require("../../models").posts;
const router = require("express").Router();
const httpStatus = require("http-status");
const validate = require("../../validation/index").validatePosts;

router.post("/", async (req, res) => {
  const body = req.body;
  const error = {
    title: validate.title({ title: body.title || "" }),
    body: validate.body({ body: body.body || "" }),
  };

  if (Object.values(error).some(err => err)) {
    return res.status(httpStatus.BAD_REQUEST).json({ error });
  }

  postsModel
    .create(body)
    .then(result => res.status(httpStatus.OK).json({ result: "Success" }))
    .catch(err =>
      res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({ error: { unknown: "Something went wrong" } })
    );
});

module.exports = router;
