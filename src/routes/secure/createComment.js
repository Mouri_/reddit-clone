const commentsModel = require("../../models").comments;
const postsModel = require("../../models").posts;
const router = require("express").Router();
const httpStatus = require("http-status");
const validate = require("../../validation/index").validateComment;

router.post("/", (req, res) => {
  const body = req.body;
  const error = {
    title: validate.title({ title: body.title || "" }),
    body: validate.body({ body: body.body || "" }),
    belongs_to: validate.belongsTo({ belongs_to: body.belongs_to || "" }),
  };

  if (Object.values(error).some(err => err)) {
    return res.status(httpStatus.BAD_REQUEST).json({ error });
  }
  commentsModel
    .create(req.body)
    .then(result => res.status(httpStatus.OK).json({ result: "Success" }))
    .catch(err => res.status(httpStatus.BAD_REQUEST).json(err));
  postsModel.increment("comments_count", {
    where: {
      id: body.belongs_to,
    },
  });
  commentsModel.increment("comments_count", {
    where: {
      id: body.belongs_to,
    },
  });
});

module.exports = router;
