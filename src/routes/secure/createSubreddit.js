const subRedditModel = require("../../models").subReddit;
const router = require("express").Router();
const httpStatus = require("http-status");
const validate = require("../../validation/index").validateReddit;

router.post("/", async (req, res) => {
  const body = req.body;
  const error = {
    title: validate.title({ title: body.title || "" }),
    about: validate.about({ about: body.about || "" }),
    url_name: validate.url({ url: body.url_name || "" }),
  };

  await checkInDB("url_name", body.url_name || "", error);

  if (Object.values(error).some(err => err)) {
    return res.status(httpStatus.BAD_REQUEST).json({ error });
  }

  subRedditModel
    .create(body)
    .then(result => res.status(httpStatus.OK).json({ result: "Success" }))
    .catch(err =>
      res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .json({ error: { unknown: "Something went wrong" } })
    );
});

async function checkInDB(prop, value, error) {
  if (error.hasOwnProperty(prop)) {
    await subRedditModel
      .findAll({
        attributes: ["url_name"],
        where: {
          [prop]: value,
        },
      })
      .then(result => {
        if (result.length !== 0)
          error[prop] = `${prop.replace("_", " ")} already exists`;
      });
  }
}

module.exports = router;
