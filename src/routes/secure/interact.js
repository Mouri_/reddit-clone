const router = require("express").Router();
const usersModel = require("../../models").users;
const postsModel = require("../../models").posts;
const httpStatus = require("http-status");

router.post("/", async (req, res) => {
  const body = req.body;
  const user = await usersModel.findByPk(body.user_name, {
    raw: true,
  });

  if (body.join_sub_reddit) {
    joinSubReddit(body, user, req, res);
  }

  if (body.vote) {
    vote(body, user, req, res);
  }
});

function joinSubReddit(body, user, req, res) {
  const updatedSubredditList = JSON.stringify(
    new Array(...JSON.parse(user.joined_sub_reddit)).concat([
      body.join_sub_reddit,
    ])
  );
  const update = { joined_sub_reddit: updatedSubredditList };

  updateUser(update, body.user_name, req, res)
    .then(result => sendSuccess(res))
    .catch(err => sendError(res));
}

async function vote(body, user, req, res) {
  const votes = JSON.parse(user.votes);
  const postId = body.vote[0];
  const postVote = body.vote[1];
  const post = await getPost(postId);

  let upVotes = post.up_votes;
  let downVotes = post.down_votes;
  let currentVote = votes[postId];

  if (currentVote < 0) {
    downVotes--;
  } else if (currentVote > 0) {
    upVotes--;
  }

  if (postVote < 0) {
    downVotes = parseInt(downVotes)+1;
    currentVote = -1;
  } else if (postVote > 0) {
    upVotes = parseInt(upVotes)+1;
    currentVote = 1;
  } else {
    currentVote = 0;
  }

  votes[postId] = currentVote;

  const userUpdate = { votes: JSON.stringify(votes) };
  const postUpdate = { up_votes: upVotes, down_votes: downVotes };

  updatePost(postUpdate, postId, req, res)
    .then(result => updateUser(userUpdate, body.user_name, req, res))
    .then(result => sendSuccess(res))
    .catch(err => sendError(res));
}

function sendSuccess(res) {
  res.status(httpStatus.OK).json({ result: "Success" });
}

function sendError(res) {
  res
    .status(httpStatus.INTERNAL_SERVER_ERROR)
    .json({ error: { unknown: "Something went wrong" } });
}

function getPost(id) {
  return postsModel.findByPk(id, {
    raw: true,
  });
}

function updatePost(update, postId, req, res) {
  return postsModel.update(update, {
    where: {
      id: postId,
    },
  });
}

function updateUser(update, user_name, req, res) {
  return usersModel.update(update, {
    where: {
      user_name,
    },
  });
}

module.exports = router;
