const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const httpStatus = require("http-status");

const { publicRoute, secureRoute } = require("./routes");
const authRoute = require("./middlewares/auth");
const timeout = require("connect-timeout");
const config = require('./config/config');
const getUserName = require("./middlewares/getUserName");

const app = express();

const corsOptions = {
  // origin: [config.origin],
  methods: ["GET", "POST"],
  allowedHeaders: ["Content-Type", "Authorization"],
};

// set security HTTP headers
app.use(helmet());

// parse json request body
app.use(express.json());

// enable cors
app.use(cors(corsOptions));
app.options("*", cors());

// Sets timeout for each request
app.use(timeout("5s"));

// Allowing public routes
app.use("/", getUserName);

// Allowing public routes
app.use("/", publicRoute);

// jwt authorization
app.use("/", authRoute);

// Allowing secure routes
app.use("/", secureRoute)

// send back a 404 error for any unknown api request
app.use((req, res) => {
  res.status(httpStatus.NOT_FOUND).json({ error: "Not found" });
});

module.exports = app;
