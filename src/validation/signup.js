const Joi = require("joi");
const { JoiPasswordComplexity } = require("joi-password")

const userNameSchema = Joi.object({
  username: Joi.string().alphanum().min(3).max(30).required(),
});

const passwordSchema = Joi.object({
  password: JoiPasswordComplexity.string().min(8).minOfSpecialCharacters(1).minOfLowercase(1).minOfUppercase(1).minOfNumeric(1).required(),
});

const emailSchema = Joi.object({
  email: Joi.string().email({
    minDomainSegments: 2,
    tlds: { allow: ["com", "net"] },
  }),
});

module.exports.userName = userName => {
    const error = userNameSchema.validate(userName).error;
    if (error) {
      return error.details[0].message.replace(/"/g, "");
    }};

module.exports.password = password => {
    const error = passwordSchema.validate(password).error;
    if (error) {
      return error.details[0].message.replace(/"/g, "");
    }};

module.exports.email = email => {
    const error = emailSchema.validate(email).error;
    if (error) {
      return error.details[0].message.replace(/"/g, "");
    }};
