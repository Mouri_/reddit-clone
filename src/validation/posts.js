const Joi = require("joi");

const titleSchema = Joi.object({
  title: Joi.string().required().min(5).max(75),
});

const bodySchema = Joi.object({
  body: Joi.string().required().min(10).max(2000),
});

module.exports.title = title => {
  const error = titleSchema.validate(title).error;
  if (error) {
    return error.details[0].message.replace(/"/g, "");
  }
};

module.exports.body = body => {
  const error = bodySchema.validate(body).error;
  if (error) {
    return error.details[0].message.replace(/"/g, "");
  }
};
