const Joi = require("joi");

const titleSchema = Joi.object({
  title: Joi.string().required().min(5).max(75),
});

const aboutSchema = Joi.object({
  about: Joi.string().required().min(10).max(10000),
});

const urlSchema = Joi.object({
    url: Joi.string().required().alphanum().min(3).max(20),
  });

module.exports.title = title => {
  const error = titleSchema.validate(title).error;
  if (error) {
    return error.details[0].message.replace(/"/g, "");
  }
};

module.exports.about = about => {
  const error = aboutSchema.validate(about).error;
  if (error) {
    return error.details[0].message.replace(/"/g, "");
  }
};

module.exports.url = url => {
    const error = urlSchema.validate(url).error;
    if (error) {
      return error.details[0].message.replace(/"/g, "");
    }
  };
  