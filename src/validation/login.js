const Joi = require("joi");

const userNameSchema = Joi.object({
  username: Joi.string().required(),
});

const passwordSchema = Joi.object({
  password: Joi.string().required(),
});

module.exports.userName = userName => {
  const error = userNameSchema.validate(userName).error;
  if (error) {
    return error.details[0].message.replace(/"/g, "");
  }
};

module.exports.password = password => {
  const error = passwordSchema.validate(password).error;
  if (error) {
    return error.details[0].message.replace(/"/g, "");
  }
};
