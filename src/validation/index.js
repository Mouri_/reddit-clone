module.exports.validateLogin = require("./login");
module.exports.validateSignup = require("./signup");
module.exports.validatePosts = require("./posts")
module.exports.validateReddit = require("./reddit")
module.exports.validateComment = require("./comments")