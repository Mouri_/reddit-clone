const Sequelize = require("sequelize");
const sequelizeConfig = require("../config/config").sequelize;

// Establishes a connection to database
const sequelize = new Sequelize(
  sequelizeConfig.databaseName,
  sequelizeConfig.user,
  sequelizeConfig.password,
  { dialect: sequelizeConfig.dialect, host: sequelizeConfig.host }
);

module.exports = sequelize;
