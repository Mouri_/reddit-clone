const sequelize = require("./");
require("../models");

// Creates table in database
sequelize
  .sync({ force: true })
  .then(res => console.log("DB creation success"))
  .then(res => sequelize.close())
  .catch(console.error);
