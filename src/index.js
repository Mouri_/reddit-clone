const app = require('./app');
const config = require('./config/config');

app.listen(config.port, () => {
    console.log("Express server running on port: ", config.port)
})