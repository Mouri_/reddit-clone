module.exports.users = require("./users");
module.exports.posts = require("./posts");
module.exports.comments = require("./comments");
module.exports.subReddit = require("./subReddit");