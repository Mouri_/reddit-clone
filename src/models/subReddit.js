const Sequelize = require("sequelize");

const sequelize = require("../database/index");

const tableColumns = {
  url_name: {
    type: Sequelize.STRING,
    allowNull: false,
    primaryKey: true,
    unique: true,
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  about: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  type: {
    type: Sequelize.STRING,
    allowNull: false,
    defaultValue: "public",
  },
  sub_reddit_image: {
    type: Sequelize.STRING,
    defaultValue: "subreddit.png",
  },
  rules: {
    type: Sequelize.TEXT,
    defaultValue: "[]",
  },
  is_NSFW: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  admin_name: {
    type: Sequelize.STRING,
  },
};

module.exports = sequelize.define("sub_reddit", tableColumns);
