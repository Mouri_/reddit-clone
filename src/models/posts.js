const Sequelize = require("sequelize");

const sequelize = require("../database/index");

const tableColumns = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  body: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  type: {
    type: Sequelize.STRING,
    allowNull: false,
    defaultValue: "post",
  },
  is_NSFW: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  image: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  video: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  poll: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  country: {
    type: Sequelize.STRING,
    allowNull: false,
    defaultValue: "IN",
  },
  sub_reddit: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  user_name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  hot: {
    type: Sequelize.VIRTUAL,
    get() {
      return (
        this.getDataValue("up_votes") +
        Math.abs(this.getDataValue("down_votes"))
      );
    },
  },
  top: {
    type: Sequelize.VIRTUAL,
    get() {
      return (
        this.getDataValue("up_votes") +
        this.getDataValue("share_count") +
        this.getDataValue("comments_count")
      );
    },
  },
  share_count: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0,
  },
  up_votes: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0,
  },
  down_votes: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0,
  },
  comments_count: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0,
  },
};

module.exports = sequelize.define("posts", tableColumns);
