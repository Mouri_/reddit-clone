const Sequelize = require("sequelize");

const sequelize = require("../database/index");

const tableColumns = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  body: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  user_name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  belongs_to: {
    type: Sequelize.INTEGER,
    allowNull: true,
  },
  share_count: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0,
  },
  up_votes: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0,
  },
  down_votes: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0,
  },
  comments_count: {
    type: Sequelize.INTEGER,
    allowNull: true,
    defaultValue: 0,
  },
};

module.exports = sequelize.define("comments", tableColumns);
