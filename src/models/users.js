const Sequelize = require("sequelize");

const sequelize = require("../database/index");

const tableColumns = {
  user_name: {
    type: Sequelize.STRING,
    allowNull: false,
    primaryKey: true,
    unique: true,
  },
  about: {
    type: Sequelize.STRING,
    allowNull: true,
  },
  status: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  },
  verification: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
  age: {
    type: Sequelize.INTEGER,
    allowNull: true,
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true,
    },
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  user_image: {
    type: Sequelize.STRING,
    defaultValue: "user.png",
  },
  user_banner: {
    type: Sequelize.STRING,
    defaultValue: "banner.png",
  },
  saved_posts: {
    type: Sequelize.TEXT,
    defaultValue: "[]",
  },
  hidden_posts: {
    type: Sequelize.TEXT,
    defaultValue: "[]",
  },
  votes: {
    type: Sequelize.TEXT,
    defaultValue: "{}",
  },
  joined_sub_reddit: {
    type: Sequelize.TEXT,
    defaultValue: "[]",
  },
};

module.exports = sequelize.define("users", tableColumns);
